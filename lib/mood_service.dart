
import 'package:intl/intl.dart';

import 'mood.dart';

var mockData = [
  Mood(date: '10-10-2020 10:10',id: 1, word: 'Today is a good day.I got bounes form my company.',emotion: 'Happy')
, Mood(date: '13-10-2020 12:20',id: 2,word: 'Today is a bad day.I lost my wallet in the afternoon.',emotion: 'Bad')];
var lastId = 3;
String getDate(){
  DateTime now = DateTime.now();
  String formattedDate = DateFormat('yyyy-MM-dd kk:mm').format(now);
  return formattedDate ;
}

int getNewId(){
  return lastId++ ;
}

Future<void> addNew(Mood mood){
  return Future.delayed(Duration(seconds: 1),(){
    mockData.add(Mood(date: getDate(),id :getNewId(), word: mood.word, emotion: mood.emotion));
  });
}

Future<void> saveMood(Mood mood){
  return Future.delayed(Duration(seconds: 1),(){
    var index = mockData.indexWhere((element) => element.id == mood.id);
    mockData[index]=mood ;
  });
}

Future<void> delMood(Mood mood){
  return Future.delayed(Duration(seconds: 1),(){
    var index = mockData.indexWhere((element) => element.id == mood.id);
    mockData.removeAt(index);
  });
}

Future<List<Mood>> getMoods(){
  return Future.delayed(Duration(seconds: 1),() => mockData);
}