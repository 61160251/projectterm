import 'package:flutter/material.dart';
import 'mood_app_widget.dart';

void main() {
  runApp(MaterialApp(
    title: 'MyMood',
    home: MainPage(),
  ));
}
