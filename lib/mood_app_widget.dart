import 'package:flutter/material.dart';
import 'package:project/mood_form_widget.dart';

import 'mood.dart';
import 'mood_service.dart';

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late Future<List<Mood>> _moods;
  var select = '';
  @override
  void initState() {
    super.initState();
    _moods = getMoods();
  }

  selectpic(String val) {
    if (val == 'Very Bad') {
      return 'images/VeryBad.png';
    } else if (val == 'Bad') {
      return 'images/Sad.png';
    } else if (val == 'Normal') {
      return 'images/Normal.png';
    } else if (val == 'Happy') {
      return 'images/Happy.png';
    } else if (val == 'Very Happy') {
      return 'images/VeryHappy.png';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[50],
      appBar: AppBar(
        title: Text('MyMood', style: new TextStyle(fontSize: 24.0,color: Colors.blue[700],fontWeight: FontWeight.bold) ),
        backgroundColor: Colors.white,
        
      ),
      body: FutureBuilder(
        future: _moods,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          List<Mood> moods = snapshot.data as List<Mood>;
          return ListView.builder(
            itemBuilder: (context, index) {
              var mood = moods.elementAt(index);

              return ListTile(
                title:
                    //     Card(
                    //     child: (ListTile(
                    //     leading: Image.asset(selectpic(mood.emotion),width: 80 ,height: 80),
                    //     title: Text(mood.emotion),
                    //     subtitle: Text('${mood.word} ${mood.date}'),
                        // trailing: IconButton(
                        //   icon: Icon(Icons.clear_outlined),
                        //   onPressed: () async {
                        //     await delMood(mood);
                        //     setState(() {
                        //       _moods = getMoods();
                        //     });
                        //   },
                        // ),
                    //   )),
                    // )
                    Card(
                      color: Colors.blue[100],
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    
                    
                    children: [Padding(padding: EdgeInsets.only(top: 6)),
                      ListTile(
                        leading: Image.asset(selectpic(mood.emotion),
                            width: 80, height: 80),
                        title: Text(mood.emotion,style: TextStyle(fontWeight: FontWeight.bold),),
                        subtitle: Text(
                          '${mood.date}',
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                        trailing: IconButton(
                          icon: Icon(Icons.clear_outlined),
                          onPressed: () async {
                            await delMood(mood);
                            setState(() {
                              _moods = getMoods();
                            });
                          },
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        
                        child: Text(
                          '${mood.word}',textAlign: TextAlign.left,
                          style:
                              TextStyle(color: Colors.black.withOpacity(1),fontSize: 18),
                        ),
                      ),),
                    ],
                  ),
                ),
                onTap:() async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context)=> MoodForm(mood: mood)));
                setState(() {
                  _moods = getMoods();
                });
                } ,
              );
            },
            itemCount: moods.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.orange,
        onPressed: () async {
          Mood newMood = Mood(id: -1, word: '', date: '', emotion: '');
          await Navigator.push(context,
              MaterialPageRoute(builder: (context) => MoodForm(mood: newMood)));
          setState(() {
            _moods = getMoods();
          });
        },
      ),
    );
  }
}
