import 'package:flutter/material.dart';

import 'mood.dart';
import 'mood_service.dart';

class MoodForm extends StatefulWidget {
  Mood mood;
  MoodForm({Key? key, required this.mood}) : super(key: key);

  @override
  _MoodFormState createState() => _MoodFormState(mood);
}

class _MoodFormState extends State<MoodForm> {
  final _formKey = GlobalKey<FormState>();
  Mood mood;
  _MoodFormState(this.mood);
  String val = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[50],
      appBar: AppBar(title: Text('MoodToday',style: new TextStyle(fontSize: 24.0,color: Colors.white,fontWeight: FontWeight.bold) ),
        backgroundColor: Colors.lightBlue[700],),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
          
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row( mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(padding: EdgeInsets.only(top: 100)),
              Image.asset('images/VeryBad.png',width: 80 ,height: 80),
              Image.asset('images/Sad.png',width: 80 ,height: 80),
              Image.asset('images/Normal.png',width: 80 ,height: 80),
              Image.asset('images/Happy.png',width: 80 ,height: 80),
              Image.asset('images/VeryHappy.png',width: 80 ,height: 80),

            ],),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Radio(
                  value: 'Very Bad',
                  groupValue: val,
                  onChanged: (String? value) {
                    setState(() {
                      val = value!;
                    });
                    mood.emotion = value!;
                  },
                ),
                new Text(
                  'Very Bad',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 'Bad',
                  groupValue: val,
                  onChanged: (String? value) {
                    setState(() {
                      val = value!;
                    });
                    mood.emotion = value!;
                  },
                ),
                new Text(
                  'Bad',
                  style: new TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                new Radio(
                  value: 'Normal',
                  groupValue: val,
                  onChanged: (String? value) {
                    setState(() {
                      val = value!;
                    });
                    mood.emotion = value!;
                  },
                ),
                new Text(
                  'Normal',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 'Happy',
                  groupValue: val,
                  onChanged: (String? value) {
                    setState(() {
                      val = value!;
                    });
                    mood.emotion = value!;
                  },
                ),
                new Text(
                  'Happy',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 'Very Happy',
                  groupValue: val,
                  onChanged: (String? value) {
                    setState(() {
                      val = value!;
                    });
                    mood.emotion = value!;
                  },
                ),
                new Text(
                  'Very Happy',
                  style: new TextStyle(fontSize: 16.0),
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(left: 10)),
            TextFormField(
              initialValue: mood.word,
              decoration: InputDecoration(labelText: 'Write your day'),
              onChanged: (String? value) {
                mood.word = value!;
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please input some text';
                }
                return null;
              },
            ),
            Padding(padding: EdgeInsets.all(10)),
            ElevatedButton(
              
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    if (mood.id > 0 ) {
                      await saveMood(mood);
                    } else{
                      await addNew(mood);
                    }
                  }
                  Navigator.pop(context);
                },
                child: Text('confirm'),
                style: ElevatedButton.styleFrom(
                  primary: Colors.lightBlue
                ),)
          ],
        )),
      ),
    );
  }
}
