class Mood {
  int id;
  String word;
  String date ;
  String emotion ;

  Mood({
    required this.id,
    required this.word,
    required this.date,
    required this.emotion,
  });

  Map<String, dynamic> toMap() {
    return {
      'date': date,
      'id': id,
      'word': word,
      'emotion': emotion,
      
    };
  }

 static List<Mood> toList(List<Map<String, dynamic>> maps){
   return List.generate(maps.length, (i){
     return Mood(
       date: maps[i]['date'],
       id: maps[i]['id'],
       word: maps[i]['word'],
       emotion: maps[i]['emotion']
       
     );
   });
 }

  @override
  String toString() {
    return 'Mood{date: $date, id: $id, word: $word, emotion: $emotion}';
  }
}